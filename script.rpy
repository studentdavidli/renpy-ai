﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

# e will be random character
define e = Character("Boring")
define f = Character('Mark')
define xd = Character('The Edge')
# f will be from the great ruler

# epic lines will be used from rewrite, muv luv alternative, maybe island.

# The game starts here.

label start:

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    scene bg room

    # This shows a character sprite. A placeholder is used, but you can
    # replace it by adding a file named "eileen happy.png" to the images
    # directory.

    show generic happy

    "So I just had to say, I know this is a very difficult story to tell."

    "I'm pretty sure the world has already decided that we are all human. We have our own beliefs, and ours are our own stories."
    
    "We have our own personal experiences of how we feel, and how we think. In fact, I've heard about a good number of people who have told me that the only way to move forward is to believe in themselves."

    "I couldn't see a single thing. I thought I was in the middle of one of the most beautiful times, but this was far beyond me."

    "A light flashed in my eyes, and I was in a very dark place. I can't say the exact moment I found myself in the middle of this world, but I felt like I could feel my whole being in my hands."

    "My body was not working. I was in a terrible pain, and I was not coming out of the room. I was in a very serious condition. I was bleeding and had no pain control. I felt like I was dreaming."

    "I felt like I was going to die. I was afraid to go to the toilet, or to the bathroom. I couldn't even get a shower. I was very scared. I remember thinking I would be in the kitchen crying."
    
    "It was really scary. I was crying. I was really upset. I was really scared. I was really scared to go out there, because I was so focused on trying to get through, I couldn't find anything." 
    
    "I was so stressed out. I was so focused on trying to get through, I couldn't find anything. It was really scary. I was just crying. I was really upset. I was really scared." 
    
    "I was really scared. I was really scared to go out there, because I was so focused on trying to get through, I couldn't find anything. I was so stressed out. I was so focused on trying to get through, I couldn't find anything."

    "I felt like I was going to die and I wanted to go to the toilet. But I was so scared, I couldn't."

    "So for a lot of people, that's probably not what they want."

    "The idea here is that you have to be aware of your own limits. I think that's the most basic thing that we all have in common."

    "Although people always told me to appreciate hard work and give thanks for making it possible for me to live my life my way."

    "Regardlessly if you work hard or not, they held me accountable and blamed me for no reason."

    "Always saying, \"The more you work, the more people will love you.\""

    "Those people in your life are treating you like a second class citizens. People are just kind of like that."

    "I did not care. I had nothing to lose, nothing to lose at all."

    "But it's not that simple. We're all human beings. We have our own personal stories, our own thoughts and our own feelings and feelings of shame and loss. And if we want to move on, we can."
    # These display lines of dialogue.
    "But I can't. I'm not ready, you know?"
    
    "I was going to fail because I was going to be alone in this world that I was going to live in."

    "I was going to fail because I was going to have to find a way to live in my own way."

    "But that's the only thing that really made me go."

    "That's the only thing that made me want to do something else."

    "I wanted to stop being a kid in a way that would make me happy."

    "To stop being an adult in the way that would make me feel good about myself."

    "This is the last time. I will not be able to take the time to finish this. I will not be able to give up. I will not be able to do anything. I will not be able to stop this. And I will not be able to stop this."

    "Let's face the fact that it's not all about violence or blood. You don't have to do anything to be good and strong, and I need to make this world a better place for all of us."

    "I started to have thoughts of everything, of everything that had been there for me and what I'd never done, to think it over."
    
    "And I just realized that I had to put myself in the role of one of those people, and that I had to be a part of it."

    "I did not feel shame. I felt shame. I felt ashamed. I felt ashamed. I felt ashamed. I felt ashamed. I looked past all of my problems to the future."

    "I am the one who knows the truth. Thus, a man, with all his might, will be able to make his own way of life as free from the shackles of the world."

    "I was my own worst enemy and I didn't want to be. This was my life, I had no interest in it. I just wanted to be a part of the community." 
    
    "I wanted to be a part of this place as much as I wanted to be here. I wanted to live in my parents' basement and live on my own. The only thing I did was try to get out of this place."

    "I've always been a bad person. I'm not that person. I'm not the person I want to be. I didn't want to be the bad person. I just tried to help people."

    "It's not that I feel I can't do this. I don't feel it can be done without me. It's that I've never been able to do what I want to."
    
    "It was not my fault. It was not my fault. It was not my fault. It was not my fault."
    
    "This was a very small group of people that were so proud of the fact that they did not have to deal with the consequences of any of the bad things that they had done."

    "I am going to die with the pain of dying. And if I die, I will be reborn in that I don't know. I think it would be very arrogant."

    "But it would be cruel and even cruelier to lose your life when you paid off your debt. So I think it would be kind of cool that we could get the word out."
    
    "The fact that they're saying that we're going to pay the debt is so great to us."

    "The only people who can accept the punishment of death, are those who choose death, and I will not be able to bear it."


    f "There was a time I had a conversation with an interesting friend." 

    f "All I'm doing is trying to get people to listen to me, and then I'm going to talk." 
    
    f "I'm doing something I've never done before, I guess. Because if I was on the road and somebody was saying something like."
    
    f "Why do you want to talk about me? and I'm just going to stop them, I'm not going to stop them. It's very, very hard."

    "I tried to do a good job." 
    
    "I said I didn't have any ideas and that I liked how I was living, that I would like to go back to school and live with my friends as I did with my parents, but that I wouldn't."

    "I'd just go back to school."

    "I tried to make some friends and make some friends and make some friends and make some friends and make some friends, but I couldn't."

    "You see, I didn't have a lot of friends at that point in my life."

    "I had lots of friends and I didn't have any friends at that point in my life."

    "My life wasn't what it is today. I was still young."

    "And I had to take it to the next level."

    "I knew that I was going to be a big part of something special, that I was going to be an amazing person, and that my life could be the perfect match for it. But I didn't know what that would be."

    "I saw that I was going to be a big part of something special."

    "I saw that I could be a person who could inspire others. And that I could be part of something that made me the best I can be."

    "And that I was going to be a person who could help others in need."

    "I saw that I could be a person that could make others happy."

    "I saw that I could be a person that could make someone happy."

    "And that I could be someone who could bring hope to people."

    "And that I could be a person that could set people on a path of transformation."

    "I saw that I could be a person that could make people change their lives."

    "And that I could be a person that could change the world."

    "I saw that I could be a person that could change the world."

    "I am not interested in what I have done to you. I am not interested in what I have done to you. I am not interested in what I have done to you.You can go and read the book that started it all, but I am going to tell." 
    
    "They are always ready to help, regardless of what they are called to do. They are the ones who inspire, and that causes fear."

    "They are the ones who provide joy and inspiration. They are the ones who build the world and help people to live within it. They are the ones who bring the light and the hope and hope that we all share." 
    
    "They are the ones who help us to live our lives with dignity and respect. They are the ones who remind us of what we are, what we have and what we have not in common."

    "They are the ones who inspire us to live our lives with courage and integrity, in a place where we feel safe, where we feel safe in our own skin, where we feel confident"
    ", where we feel confident that we are doing something right."

    "I can’t hold back any regret or sadness for those I’ve left behind. I can't take comfort in knowing that this is the time to step back and let go of any regrets and bitterness that have come to overwhelm me and make things better."
    
    "I can't leave the burden of my past. I can’t move forward without feeling ashamed. I can’t feel compassion for those I have left behind. I can't lose pride in my accomplishments. I can't feel hope for the future."

    "I can't give myself the courage to make the decision to leave behind those that I have made.I can’t take the time to see a new way of being myself. I can't look back on those I have left behind."

    "They didn't take my hand. They didn't take my love. They didn't take my heart. They took the last one out of me."

    "I still remember what I felt. I still remember that I was a child. That I had no desire to be anything. So I stopped trying to be anything."

    "I started to change."

    "But then they started to stop making me feel."

    "The best way to deal with such things is to do something about it. People who have been there, done it, are the ones who have the courage to do it."

    "They called me a coward and a loser. They were mad because I was doing the right thing."

    "I was a failure. If people had asked me about it, I would have said that I was doing the right thing."

    "I'm not going to be the first person to say that, but let me make it clear: I'm not here to say that anyone should feel bad about the way I handled myself."
    
    "I'm not here to say that I'm not a terrible person. I'm not here to say that I'm a fool. But I'm here to say that I'm not a loser."

    "The one who has the power to change everything will be the one who will become the one who will not change anything."

    "To become the one who has the power to change the world, you must first be able to make those changes."

    "If you are able to make those changes, you will become the one who will be able to change everything."

    "I've got to get on with it, I can't wait to see what happens next. In the beginning, I just wanted to get back on track, and then I realized, I don't want to be in any sort of rush like I was before."
    
    "I couldn't imagine what I was going through. In the beginning, I just wanted to get back on track, and then I realized, I don't want to be in any sort of rush like I was before."
    
    "I couldn't imagine what I was going through. I didn't want to be in any sort of rush like I was before."
    
    "I did not think I could have done anything that was a disgrace to anybody, and I had not done anything that was a disgrace to anybody. I had never done anything that was a disgrace to anyone." 
    
    "I had never done anything that was a disgrace to anyone."

    "The struggle for freedom is not only a struggle of the oppressed, but also of the oppressed. We must recognize that the struggle is the struggle of the oppressed." 
    
    "We must recognize that the struggle is the struggle of the oppressed. We must recognize that the struggle is the struggle of the oppressed. We must recognize that the struggle is the struggle of the oppressed."

    "I think I have a lot of confidence in myself, and I can handle that. I've got a lot of confidence in myself."
    
    "I know that you can't go out and get your fair share of criticism and you can't go out and say, 'It's my fault. I'm not going to be able to do this. It's my fault.' I don't think that's true." 
    
    "I think that people are going to get their fair share of criticism and I think people are going to be willing to accept that."

    "I'm not going to give up. I'm not going to give up. The only thing I want to do is get out of this situation. The only thing I want to do is get out of this situation. The only thing I want to do is get out of this situation."
    
    "That's a very different feeling to my own experience. I'm not necessarily happy with it or I'm not happy with how my life went. I'm not happy with it because I'm unhappy with my own life. I want to live my life the way it is now, with no regrets or no regrets."
    
    "It's not possible. It's not a possibility. I want to live my life the way it is now, with no regrets or no regrets. It's not a possibility." 
    
    "I want to live my life the way it is now, with no regrets or no regrets. It's not a possibility. I want to live my life the way it is now, with no regrets or no regrets. It's not a possibility."
    
    "And I think what I'm seeing now is that people are coming out of the dark. They are, you know, they're starting to realize just how deep a hole there is in our society and how deeply the pain that we have in our lives is taking people." 
    
    "And I think that the problem that we face is not just about the pain and the pain of others. It's about the way people are treated. There's a huge amount of pain that goes on within our society."
    
    "And I think people need to understand that it is not just about the government. It is about the way that people treat one another."

    "The human mind is a collection of neurons that are connected to a single neurons at the base of the brain. You have thousands of them, and thousands of them, and they all work together to form the mental images of a person."
    
    "It's a collection of neurons that get together and connect in this person. The brain is a very, very different animal — it's not a very sophisticated animal."

    "So there are different ways for you to think about it. You might think that because of the fact that your body is wired to do things that we naturally do, that's natural, you're not going to get any kind of response from your brain. That's not true."

    "The mind's not just a collection of neurons, it's a collection of neural connections. And it's connected to all of that."

    "It's connected with all of that."

    "The brain doesn't just have neurons that are connected to the same neurons, it has the same neurons that are connected to the same neurons."

    "So the brain is a collection of neurons that are connected to all of that. It's connected to all of that."

    "Now, if you think about it, maybe it's not really a collection of neurons. But it's connected to all of that."

    "And that's why it's so important for us to remember that each of these neurons is just a collection of neurons."

    "So you can think about some of these neurons individually. And, you know, it's one of the things that's great about the human mind: There are so many neurons in it that matter."

    "I'm not going to go into the specifics of how I feel about my life because I don't want to waste time explaining my feelings and feelings and my feelings and my feelings and my feelings and my feelings and my feelings;" 
    
    "I want to be clear about the fact that I'm not trying to change people's lives."
    
    "I'm trying to do better because I feel like I'm doing something that makes me better and I'm doing something that makes me worse."

    "We can move on and not get depressed if we're depressed. We can move on and not have feelings of shame if we're not depressed. We can get better at expressing ourselves if we're not feeling bad about ourselves. And this is good for you."

    "I'm not going to go into the specifics of how I feel about my life because I don't want to waste time explaining my feelings and feelings and my feelings and my feelings and my feelings and my feelings and my feelings;" 
    
    "I want to be clear about the fact that I'm not trying to change people's lives. I'm trying to do better because I feel like I'm doing something that makes me better and I'm doing something that makes me worse."

    "This is one of those questions I was wondering about. When I first started to think about this, I thought, 'Wow, that's a nice question.' There are a lot of things people are going through."
    
    "I think there are some things that are going to take some time to sort out. But if we really go through this journey, it's going to take us a long time. And if I don't, it's going to take us a long time to move forward."

    "We can't even tell people what we think about ourselves if we're not completely aware of our own self-destructive behaviors and behaviors."
    
    "And if we're not fully aware of how we feel, we can't stop ourselves from doing things in the moment that would make us miserable. That would be a huge mistake."

    "I don't think we can stop ourselves from doing things in the moment that would make us miserable."

    "Maybe we could, but we don't know. I've seen this happen."

    "We're all human beings. We have our own personal stories, our own thoughts and their own feelings and feelings of loss and pain. And if we want to move on, we can. We're all human beings."

    "So if you're one of us, and you feel that you're stuck in a cycle of loss, what can you do?"

    "We don't have time to talk about it with you. We have time to listen to you. We have time to listen to you. We have time to talk about what we're feeling. We have time to listen to you."
    
    "We have time to talk about what we're feeling. We have time to talk about what we're feeling. We have time to talk about what we're feeling."

    "When you start to feel you're in a state of loss, what can you do?"

    "When you start to feel you're in a state of despair, what can you do?"

    "We are human beings and we have our own personal stories, our own thoughts and our own feelings and feelings of shame and loss. And if we want to move on, we can." 
    
    "We're all human beings. We have our own personal stories, our own thoughts and their own feelings and feelings of loss and pain. And if we want to move on, we can. We're all human beings."
    
    "We have our own personal stories, our own thoughts and their own feelings and feelings of despair. And if we want to move on, we can. We're all human beings." 
    
    "We have our own personal stories, our own thoughts and their own feelings and feelings of despair. And if we want to move on, we can."

    "We've all experienced the same thing. It's called the 'loss of motivation'. It's called 'the loss of self'. We've all experienced the same thing." 
    
    "It's called the 'loss of motivation'."

    "So how do you deal with it?"

    "You're not going to get through it. You're not going to get through it."

    "And then you start to think it's okay to fall in love with someone who is not suffering, to fall in love with someone who is suffering, and you're right there."
    
    "But then you start to feel like, 'What if I had a brother who was suffering?' What if he was struggling to survive? What if he was struggling so desperately to be loved?"

    "We have to get through this. We have to accept that we can never get through it and that we might have to make some compromises to make it better."

    "I'm not saying you have to accept it. But to me, that's what the process is about. It's about how we deal with loss. It's about how we handle our own feelings of shame."
    
    "And when we do, we're dealing with a person who is not, and we need to get through it, and we need to get through it."

    "We're not supposed to have a problem with that. We are supposed to be a person who is okay. We are supposed to be a person who is okay." 
    
    "And we're not supposed to be in a position of being seen as being 'good' or 'bad.'"

    "And that's going to be hard."

    "If you are the truth, then you are what you believe. If you are the lie, then you are what you believe in."
    
    "If you are the truth, then you are what you believe in."

    "It is not the truth, it is not the truth. A lie is a lie. I can never accept the truth. I cannot accept this truth." 
    
    "I can only accept the truth. I will not accept the truth. I will not accept the truth."

    "In other words, the truth is not the truth. The truth is not what you believe. The truth is what you think you know." 
    
    "The truth is what you believe in. You believe in what you believe in, and believe in what you believe in."

    "I think that's why so many people are so frustrated with us. Because of what we are saying. Because of what we are saying." 
    
    "Because we're saying so many things, so many different things, that are so offensive and so ridiculous."

    "And, so many people are so frustrated with us, and so many others are so angry."

    "It is a question that most people have asked themselves since the beginning. Many people have asked themselves how they can win." 
    
    "It is a question that many people have also asked themselves, and it is one that many people have asked themselves."

    "The problem is that the strong can win even if they are weak enough. They can win if they are strong enough, and they can win if they are weak enough."

    "It is true that the weakling's ability to win is limited. It can only be a strong, but it cannot win unless others are weak. No other living creature can survive a strong-minded person's strength."

    "And so many people are so frustrated with us because they're looking at us, and they're looking right at us. And so many people are angry because they feel we're not getting through it."
    
    "The most common response to an emotional situation is that you'll regret the situation for a long time. This is not true. It's the same with sadness and fear." 
    
    "Just as with grief and suffering, we all have our emotions. But this doesn't mean we can't still be sad about things, we have to be sad."

    "Most people who have feelings of sadness will probably tell themselves: 'I'm going to feel bad about it if I don't get it done. I'm going to feel guilty if I don't do it, but I'll still be sad about it.'"
    
    "You're not going to want to do anything about that. So when you feel sad and feeling guilty, you are not actually sad about something. You are just feeling sad and feeling guilty."

    "This is not to say that sadness is bad. It can be really bad. It can just be a very bad feeling."

    "The most common response to an emotional situation is that you'll regret the situation for a long time. This is not true. It's the same with sadness and fear." 
    
    "Just as with grief and suffering, we all have our emotions. But this doesn't mean we can't still be sad about things, we have to be sad."

    "Most people who have feelings of sadness will probably tell themselves: 'I'm going to feel bad about it if I don't get it done. I'm going to feel guilty if I don't do it, but I'll still be sad about it.'" 
    
    "You're not going to want to do anything about that. So when you feel sad and feeling guilty, you are not actually sad about something. You are just feeling sad and feeling guilty."

    "This is not to say that sadness is bad. It can be really bad. It can just be a very bad feeling."

    "You should feel the pain of your wounds. You should feel the pain of your sorrows. The pain of your wounds is unbearable." 
    
    xd "Be respectful of others. Be respectful of your fellow human beings. Do not just be the victim of others. Be an example of who you really are."

    "The pain of your sorrows is unbearable. There is no way to find peace. There is no way to be joyful. There is no way to be happy. There is no way to be joyful." 
    
    "Everything is a lie. Everything is a lie. That's what I will say. I will say more, and you will be able to see my point of view. I will say more, and you will be able to see my point of view."

    "I will have to say more. So you have to feel the pain of your wounds. There is no way to find peace. There is no way to be joyful." 
    
    "There is no way to be happy. There is no way to be joyful. I am not allowed to believe everything that I have heard. When I do this, I will feel the pain of my wounds, and I will feel the pain of my sorrows." 
    
    "The pain of your wounds is unbearable. There is no way to find peace. There is no way to be happy. I am not allowed to believe everything that I have heard. When I do this, I will feel the pain of my wounds, and I will feel the pain of my sorrows." 

    "I am not allowed to believe everything that I have heard. I must be able to do something, even if it is a lie."

    "At the same time, I am trying to find a way to make it all right before I leave. If I can't find a way to make it all right before I leave, then let's wait until I have some other way to make it all right before I leave."


    "They don't want to let their mistakes ruin their career. They want to be a better person. And for that, they need to be open and honest about their mistakes."

    "I have to say I have been a bit of a jerk. At first I didn't believe it was okay to be a jerk. I was so confused and upset by it. I was so frustrated that I was ashamed to be a jerk." 
    
    "I wanted to do something positive. I wanted to start a new chapter in my life. I wanted to be a hero."

    xd "You know heros die early."

    xd "They never cry or groan over what they have done. They never regret or agonize over what happened to them. They never regret what they have done. They never regret what they wish to accomplish."

    xd "How they differ from me."

    xd "The fact that you don't know what you’ve done tells me that you didn't do it because you didn've never done it.‬"

    xd "You did it because you wanted to be something big, and you were scared to even be in front of that big, and you wanted to be something special, and you could never been able to achieve that.‬"

    "People are so great when you work hard, you can be the best you can be. You can be the best you can be."

    xd "You didn’ve done it because you were tired of being the one who didn’ve done it.‬"

    xd "You didn’ve done it because you thought it would be too fun to do it.‬"

    "I wanted to do something positive for my community. I wanted to be recognized for my achievements. I wanted to be part of something great." 
    
    "I wanted to be a part of a community that would allow me to be a good person. I wanted to be part of something that would allow me to be a good person." 
    
    "The moment I realized how much I've changed in the past few years, I was able to start to let go of my shame."

    "But what was the lesson?"

    "I didn't know what to do. I was in such a terrible place. I was in a bad place. I was feeling trapped." 
    
    "I was suffering from a terrible, crippling depression. I needed a break from my depression. I needed a break from my depression."
    
    "But I didn't know what to do. I needed to be there."

    "It is not possible to stop thinking of myself as a complete failure, but as a total success. I can't do anything else until I get to the point where I could get what I want." 
    
    "I have my own vision, my own priorities. I can't let it down on myself. I have to take responsibility for it. I have to be the best I can be."

    "I just hope to help others find happiness, and I want to leave something to be desired for myself."

    xd "I suppose there are things even someone like you can enjoy from life."

    "The first thing that can change is the colour of the sky." 
    
    "This is the sign of the year that we call the season, the day in which the sun rises, and the moon moves from its usual spot in the sky to its usual place in the night." 
    
    "The night sky is always clear, but the day is always bright – the season is always just for us."

    "So we must come back and see if we can make a new year by using the year-round or autumn sky." 
    
    "If we can't, then it's time to move on, because a new year means we have to wait to see the end of the rainbow."

    "At night, in the middle of the forest, you will hear the sounds of birds playing, and you will see the sound of the wind blowing." 
    
    "You have the opportunity to see the difference between the sound of birds and the sound of trees. Even in the middle of the forest, there are many different kinds of birds, and it is clear that they do not always do so well."

    "At night, you will not need to go to sleep, because the night will be just as quiet as the day. That is the way it is with summer."

    "There is a certain kind of love that goes on in autumn, and that is love that is of a kind of beautiful, wonderful, and wonderful colour. It is something that is so beautiful, and is so beautiful that it is very important to see it."

    "If you look very closely, you will see that there are many beautiful autumn trees. And you cannot look at them only at the end of the first day, because the day will change at the beginning of the next day."

    "My goal as I started this project was not to make something that will be seen by many people, but rather to create something that will be more than a dream of myself, and I am happy to do that."

    "In addition to being a dream of my own, I'm also happy to create an art project that will hopefully inspire people to think of what it means to be a human being, and a life that is not only different from what we have experienced, but also that we should all be happy."

    "I wish this project would inspire you to live a life of courage, and to live a life of love, and to be happy and fulfilled."
    
    xd "It may sound like a great book, and maybe you might want to read it once in a while, but I can tell you that at the end of the day, it's the best book I've read. If you're reading this right now, then you'll be able to find one of the best books about life that I've read. It's a fantastic book, and I hope you'll pick it up if it's up for sale."
    
    "There are times when you're just waiting for the right thing to happen."

    xd "Be willing to change your mind. Be willing to change."

    "Be open to new ideas and new ways of thinking. Remember that you are always learning."
    "But, I have to go. It is very, very late."
    #e "You've created a new Ren'Py game."
    
    #e "Once you add a story, pictures, and music, you can release it to the world!"

    # This ends the game.
    window hide
    
    jump credits
    return


label credits:
    $ credits_speed = 25
    scene black
    show credits_image at Move((0.5, 1.0), (0.5, -1.0), credits_speed,
                  xanchor=0.5, yanchor=0)
    with Pause(credits_speed+10)

label end:

    e "Thank you for viewing this tutorial."

    e "If you'd like to see a full Ren'Py game, select \"The Question\" in the launcher."

    e "You can download new versions of Ren'Py from {a=https://www.renpy.org/}https://www.renpy.org/{/a}. For help and discussion, check out the {a=https://lemmasoft.renai.us/forums/}Lemma Soft Forums{/a}."

    e "We'd like to thank Piroshki for contributing my sprites; Mugenjohncel for Lucy, the band, and drawn backgrounds; and Jake for the magic circle."

    e "The background music is \"Sunflower Slow Drag\", by Scott Joplin and Scott Hayden, performed by the United States Marine Band. The concert music is by Alessio."

    show eileen vhappy

    e "We look forward to seeing what you create with Ren'Py. Have fun!"

    window hide

    # Returning from the top level quits the game.
    return
